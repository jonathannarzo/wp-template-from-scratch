<?php get_header(); ?>
	<div class="site-content clearfix">
		<?php
			if (have_posts()) :
				while (have_posts()) :
					the_post();
					the_content();
				endwhile;
			else :
				echo '<p>No Content found</p>';
			endif;
		?>
		<div class="home-columns clearfix">
			<div class="one-half">
				<?php
					// The Category
					$theCat = new WP_Query('cat=4&posts_per_page=2');
					if ($theCat->have_posts()) :
						while ($theCat->have_posts()) : $theCat->the_post();
				?>
							<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
							<?php the_excerpt(); ?>
				<?php
						endwhile;
					else :

					endif;
					wp_reset_postdata();
				?>
			</div>
			<div class="one-half last">
				<?php
					// Uncategorized
					$uncat = new WP_Query('cat=1&posts_per_page=2');
					if ($theCat->have_posts()) :
						while ($uncat->have_posts()) : $uncat->the_post();
				?>
							<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
							<?php the_excerpt(); ?>
				<?php
						endwhile;
					else :

					endif;
					wp_reset_postdata();
				?>
			</div>
		</div>

	</div>
<?php get_footer(); ?>