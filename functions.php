<?php

function siteResources()
	{
		wp_enqueue_style('style', get_stylesheet_uri());
	}
add_action('wp_enqueue_scripts', 'siteResources');

// Get top ancestor
function get_top_ancestor_id()
	{
		global $post;
		if ($post->post_parent)
		{
			$ancestors = array_reverse(get_post_ancestors($post->ID));
			return $ancestors[0];
		}

		return $post->ID;
	}

// Does page have children
function has_children()
	{
		global $post;
		$pages = get_pages('child_of='. $post->ID);
		return count($pages);
	}

// Customize excerpt length
function custom_excerpt_length()
	{
		return 25;
	}
add_filter('excerpt_length', 'custom_excerpt_length');

// Theme Setup
function siteSetup()
	{
		// Navigation Menus
		register_nav_menus(array(
			'primary' 	=> __('Primary Menu'),
			'footer' 	=> __('Footer Menu')
		));

		// Add Featured Image support
		add_theme_support('post-thumbnails');
		add_image_size('small-thumbnail', 180, 120, true);
		add_image_size('banner-image', 920, 210, array('left', 'top'));

		// Add post format support
		add_theme_support('post-formats', array('aside', 'gallery', 'link'));
	}

add_action('after_setup_theme', 'siteSetup');

// Add Widget Locations
function widgetsInit()
	{
		register_sidebar(array(
			'name' 	=> 'Sidebar',
			'id' 	=> 'sidebar1',
			'before_widget' => '<div class="widget-item">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<h4 class="my-class">',
			'after_title' 	=> '</h4>'
		));
		register_sidebar(array(
			'name' 	=> 'Footer Area 1',
			'id' 	=> 'footer1'
		));
		register_sidebar(array(
			'name' 	=> 'Footer Area 2',
			'id' 	=> 'footer2'
		));
		register_sidebar(array(
			'name' 	=> 'Footer Area 3',
			'id' 	=> 'footer3'
		));
		register_sidebar(array(
			'name' 	=> 'Footer Area 4',
			'id' 	=> 'footer4'
		));
	}
add_action('widgets_init', 'widgetsInit');

// Customize Appearance Options
function siteCustomizeRegister( $wp_customize )
	{
		// Link Color
		$wp_customize->add_setting('site_link_color', array(
			'default' 	=> '#006ec3',
			'transport' => 'refresh',
		));

		// Button Color
		$wp_customize->add_setting('site_btn_color', array(
			'default' 	=> '#006ec3',
			'transport' => 'refresh',
		));

		// Standar Color Section
		$wp_customize->add_section('site_standard_colors', array(
			'title' 	=> __('Standard Colors', 'TheTheme'),
			'priority' 	=> 30
		));

		// Link Color Control
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'site_link_color_control', array(
			'label' 	=> __('Link Color', 'TheTheme'),
			'section' 	=> 'site_standard_colors',
			'settings' 	=> 'site_link_color'
		)));

		// Button Color Control
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'site_btn_color_control', array(
			'label' 	=> __('Button Color', 'TheTheme'),
			'section' 	=> 'site_standard_colors',
			'settings' 	=> 'site_btn_color'
		)));
	}
add_action('customize_register', 'siteCustomizeRegister');

// Output Custom CSS
function siteCustomizeCss()
	{
		echo '
		<style type="text/css">
			a:link,
			a:visited {
				color: '. get_theme_mod('site_link_color') .';
			}

			.site-header nav ul li.current-menu-item a:link,
			.site-header nav ul li.current-menu-item a:visited,
			.site-header nav ul li.current-page-ancestor a:link,
			.site-header nav ul li.current-page-ancestor a:visited {
				background-color: '. get_theme_mod('site_link_color') .';
			}

			div.hd-search .search-submit {
				background-color: '. get_theme_mod('site_btn_color') .';
			}
		</style>
		';
	}
add_action('wp_head', 'siteCustomizeCss');